import nose
from unittest import TestCase

from src.infrastructure.UserDataAccess import UserDataAccess


class TestUserInput(TestCase):

    def test_when_saving_with_valid_key_and_account_key(self):
        payload = '{"email": "b25@testmail.com", "key": "S"}'
        return_value = UserDataAccess().update_account_key('S', 'b25@hotmail.com')
