# bovaa.accountkey.requester

This is a lambda function that is triggered by an SNS message that is published by the bovaa.userupdated.publisher lambda

# Sample SNS message body
````
'{"email": "hi1@testmail.com", "key": "U"}
````

# Environment Variables required by the lambda
````
aws_region
aws_access_key_id
aws_secret_access_key
db_name
db_username
db_host
db_password
account_service_get_account_key
account_key_requested_topic
````

# How the lambda works
* This lambda reads the message body of the SNS that trigger it, and retrieves the email and key specified in the body.
* This is used to request an account key from the Account Service specified by the URL in the Environment Variable account_service_get_account_key.
* If the service doesn't respond, or it fails, this lambda will publish a message to the SNS queue specified by the Environment Variable account_key_requested_topic along with a delay
* If the service does respond with an account key, an update will be made to the db specified by the Environment Variables db_name, db_username, db_password, db_host
* The credentials aws_region, aws_access_key_id and aws_secret_access_key are required to publish to SQS

Sample body of the SNS queuethat triggers this lambda:
```
'{ "email": "b31@testmail.com",  "key": "v"}
```

