import mysql.connector
from src.entities.User import User
import os

class UserDataAccess:
    conf = {
        'user': os.environ['db_username'],
        'password': os.environ['db_password'],
        'host': os.environ['db_host'],
        'database': os.environ['db_name']
    }

    def update_account_key(self, key, account_key):
        cursor = None
        conn = None
        return_value = False
        try:
            conn = mysql.connector.connect(**self.conf)
            cursor = conn.cursor()
            cursor.callproc("save_account_key", (key, account_key) )

            cursor.close()
            conn.commit()
            conn.close()
            return_value = True

            return return_value

        except Exception as e:
            print("Error: ")
            print(e)
            if cursor:
                cursor.close()
            if conn:
                conn.close()
            raise e