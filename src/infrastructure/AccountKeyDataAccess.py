import json
import requests
import time
import os

class AccountKeyDataAccess:

    def _get_json_body(self, email, key):
        return {'email': email, 'key': key}

    def _get_account_key_from_response(self, content):
        content_str = content.decode('utf-8')
        content_json = json.loads(content_str)

        account_key = content_json.get('email', None)

        return account_key

    def get_account_key(self, email, key):
        json_body = self._get_json_body(email, key)
        url = os.environ['account_service_get_account_key']
        headers = {'Accept': 'application/json'}

        response = requests.post(url, headers=headers, json=json_body)
        if (response.status_code == 200):
            return self._get_account_key_from_response(response.content)
        else:
            return None