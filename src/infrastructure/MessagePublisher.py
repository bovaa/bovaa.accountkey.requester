import boto.sqs
import json
from boto.sqs.message import Message
import os

class Publisher:
    def __init__(self, aws_region, aws_access_key_id, aws_secret_access_key):
        self.client = boto.sqs.connect_to_region(aws_region,
                                                 aws_access_key_id=aws_access_key_id,
                                                 aws_secret_access_key=aws_secret_access_key)



    def publish_message(self, email, key):
        q = self.client.create_queue(os.environ['account_key_requested_topic'])
        print(q)
        message_body = json.dumps({"email": str(email), "key": str(key)}, default=lambda o: o.__dict__, sort_keys=True, indent=4)
        print(message_body)
        m = Message()
        m.set_body(message_body)
        q.write(m)
