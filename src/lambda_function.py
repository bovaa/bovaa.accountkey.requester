from src.infrastructure.AccountKeyDataAccess import AccountKeyDataAccess
from src.infrastructure.UserDataAccess import UserDataAccess
import os
from src.entities.User import User
from src.infrastructure.MessagePublisher import Publisher
import json

def _get_user_from_event(message):
    try:
        print('loading json for data {}'.format(message))
        data = json.loads(message)
        print('getting email and key')
        email = data.get('email')
        key = data.get('key')
        print(email)
        print(key)
        return (User(key, email))
    except Exception as e:
        print(e)

def lambda_handler(event, context):
    try:
        print(event)
        event_message = event['Records'][0]['Sns']['Message']
        user = _get_user_from_event(event_message)
        account_key = AccountKeyDataAccess().get_account_key(user.email, user.key)
        print(account_key)

        if account_key is None:

            publisher = Publisher(settings.env['aws_region'], settings.env['aws_access_key_id'],
                  settings.env['aws_secret_access_key'])
            publisher.publish_message(user.email, user.key)
        else:
            UserDataAccess().update_account_key(user.key, account_key)
    except Exception as e:
        print('Error. Need to be reviewed')